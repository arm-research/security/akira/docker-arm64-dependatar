# README

## Requirements
Please log into gitlab docker registry!
`docker login registry.gitlab.com -u <USERNAME> -p <ACCESSTOKEN>`

Alternatively modify the `FROM` line in dockerfile: 
If you have an `ubuntu:18.04` image currently pulled you will need to do the following:

```shell
rmi ubuntu:18.04 # remove ubuntu image
docker pull --platform=linux/amd64 ubuntu:18.04
docker tag ubuntu:18.04 ubuntu/amd64:18.04
docker rmi ubuntu:18.04
docker pull --platform=linux/arm64 ubuntu:18.04
docker tag ubuntu:18.04 ubuntu/arm64:18.04
docker rmi ubuntu:18.04
docker pull ubuntu:18.04
```

## Intro

This workflow simplifies locating and tarring up all dependencies for an Arm64 app installed with `apt`. `helper.sh` has bits for grabbing all the dependencies and gets mapped to roots `.bashrc`. Feel free to add other helper bits there. Next major thing is to mount a local bind volume for dumping the output tarfiles. 

```
mkdir -p treasuretrove
docker build --platform=linux/arm64 . -t arm-dependatar
```

```
# Easy way ->  shows up in treasuretrove folder
docker-compose run dependatar tar_dependencies whateveryouwant
# May require you to export some env vars for python
docker run -it --platform linux/arm64 -v $(realpath treasuretrove):/treasuretrove arm-dependatar /bin/bash
```

Once you have the container built, you can apt install whatever crap you want and tar up the dependencies with the `get_depends()` function.

```
# Inside container
apt install -y whateveryouwant
tar czvf treasuretrove/whateveryouwant.tgz $(get_depends whateveryouwant)
```

Or run

```
docker run -it --platform linux/arm64 -v $(realpath treasuretrove):/treasuretrove arm-dependatar tar_dependencies whateveryouwant
```

## Troubleshooting

If the Docker on Arm bits fail try this

FROM the docs:
```
Install the qemu instruction emulation to register Arm executables to run on the x86 machine. For best results, the latest qemu image should be used. If an older qemu is used some application may not work correctly on the x86 hardware. 
```
```
docker run --rm --privileged docker/binfmt:820fdd95a9972a5308930a2bdfb8573dd4447ad3
```

## Shamelessly stolen docs

### Install Docker
[Original Post](https://www.docker.com/blog/getting-started-with-docker-for-arm-on-linux/)

I had best luck installing directly from test
```shell
$ sudo apt-get update
$ sudo apt-get upgrade
$ curl -fsSL test.docker.com -o get-docker.sh && sh get-docker.sh
```

Add the current user to the docker group to avoid needing sudo to run the docker command:

```shell
$ sudo usermod -aG docker $USER 
```

### Install buildx for multi-architecture image builds

There are three options to get buildx on Linux:

    Use buildx directly from the test channel version of Docker
    Download a binary release of buildx and copy it to the $HOME/.docker directory
    Download, build, and install buildx from github.com


#### Use buildx from Docker test channel (EASIEST)

The test version of Docker already has buildx included. The only thing needed is to set the environment variable to enable experimental command line features.

```shell
$ export DOCKER_CLI_EXPERIMENTAL=enabled
```

#### Download a binary release

Another way to get buildx is to download a binary release from github and put in the .docker/cli-plugins directory.

For example, download the buildx for Linux amd64 with a browser from: https://github.com/docker/buildx/releases/tag/v0.2.0

Then copy it to the cli-plugins/ directory (create it first if necessary):

```shell
$ cp buildx-v0.2.0.linux-amd64 ~/.docker/cli-plugins/docker-buildx
```

#### Download, build, and install buildx

Because buildx is a new command and documentation is still catching up, github is a good place to read more information about how buildx works.

To get buildx from github use the commands:

```shell
$ export DOCKER_BUILDKIT=1
$ docker build --platform=local -o . git://github.com/docker/buildx
$ mkdir -p ~/.docker/cli-plugins
$ mv buildx ~/.docker/cli-plugins/docker-buildx
```

To confirm buildx is now installed run the help and the version command.

```shell
$ docker buildx --help 
```
```
Usage:  docker buildx COMMAND
Build with BuildKit

Management Commands:
  imagetools  Commands to work on images in registry

Commands:
  bake        Build from a file
  build       Start a build
  create      Create a new builder instance
  inspect     Inspect current builder instance
  ls          List builder instances
  rm          Remove a builder instance
  stop        Stop builder instance
  use         Set the current builder instance
  version     Show buildx version information 

Run 'docker buildx COMMAND --help' for more information on a command.
```

```shell
$ docker buildx version
github.com/docker/buildx v0.2.0-36-g4e61674 4e61674ac805117794cc55475a62efdef0be9818
```
