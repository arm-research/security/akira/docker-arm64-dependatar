FROM registry.gitlab.com/arm-research/security/akira/ubuntu/arm64:20.04

RUN apt update
RUN apt-get install -y stress stress-ng python3 python3-pip
RUN pip3 install click

RUN export LC_ALL=C.UTF-8 && export LANG=C.UTF-8
COPY helper.sh /root/.bashrc
COPY helper.sh .
RUN chmod a+x helper.sh
COPY tar_dependencies.py /usr/bin/tar_dependencies
RUN chmod a+x /usr/bin/tar_dependencies
RUN mkdir /treasuretrove
VOLUME /treasuretrove

# ENTRYPOINT ["apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances stress-ng | grep \"^\w\" | sort -u | xargs -n1 dpkg -L | sort | uniq | xargs -n1 -i sh -c 'test -f {} && echo {}' | tar czvf stress-ng^Cgz -T -"]
