#!/usr/bin/env python3
import click
import subprocess
import sys

@click.command()
@click.argument("PACKAGES", nargs=-1, type=str)
def dependatar(packages):
    packages = list(packages)
    cmd = "apt install -y %s" % " ".join(packages)
    r = subprocess.run(cmd, shell=True)
    for trgt in packages:
        cmd = ". ./helper.sh && tar czvf /treasuretrove/%s.tgz $(get_depends %s)" % (trgt, trgt)
        r = subprocess.run(cmd, shell=True)



if __name__ == '__main__':
    dependatar()

