#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH

get_depends_base() {
	# local outfile="${1}.tgz"
	for pkg in $*; do
		apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances $pkg | grep "^\w" | sort -u | xargs -n1 dpkg -L | sort | uniq | xargs -n1 -i sh -c 'test -f {} && echo {}' #| tar czvf $outfile -
	done
}

# Some of this crap is simlinked, so lets dump what it links to first and let tar handle the rest
get_depends() {
  for f in $(get_depends_base $*); do
    if [ -L ${f} ] ; then
      echo "$(dirname $f)/$(readlink $f)";
      echo "$f";
    else
      echo "$f";
    fi;
  done
}

